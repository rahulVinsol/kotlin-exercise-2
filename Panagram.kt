fun main(args:Array<String>){
	val sentence:String;
	val panagramSet:Set<Char>
	panagramSet = HashSet<Char>()
	panagramSet.addAll('a'..'z');
	sentence = args[0];
	for(ch in sentence)
		panagramSet.remove(ch.toLowerCase());
	if(panagramSet.isEmpty())
		println("Panagram");
	else
		println("Not Panagram");
}
